Name:                decentxml
Version:             1.4
Release:             2
Summary:             XML parser optimized for round-tripping and code reuse
License:             BSD-3-Clause
URL:                 https://bitbucket.org/digulla/decentxml/src/default/
Source0:             https://repo1.maven.org/maven2/de/pdark/decentxml/1.4/decentxml-%{version}-src.zip
Source1:             https://repo1.maven.org/maven2/de/pdark/decentxml/1.4/decentxml-1.4-src.zip
BuildRequires:       maven-local mvn(junit:junit)
BuildRequires:       mvn(org.apache.maven.plugins:maven-assembly-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-source-plugin)
BuildArch:           noarch

%description
XML parser optimized for round-tripping and code reuse with main
features being:
 * Allows 100% round-tripping, even for weird whitespace between
   attributes in the start tag or in the end tag
 * Suitable for building editors and filters which want/need to
   preserve the original file layout as much as possible
 * Error messages have line and column information
 * Easy to reuse individual components
 * XML 1.1 compatible

%package javadoc
Summary:             API documentation for %{name}

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q
unzip %{SOURCE1}
ln -sf %{name}-%{version}/xmlconf ../xmlconf
sed -i -e "s|junit-dep|junit|g" pom.xml
sed -i '/not_wf_sa_16[89] /d' src/test/java/de/pdark/decentxml/XMLConformanceTest.java
%pom_remove_plugin :maven-javadoc-plugin
%pom_xpath_set "pom:plugin[pom:artifactId='maven-assembly-plugin']/pom:executions/pom:execution/pom:goals/pom:goal[text()='attached']" single

%build
%mvn_file  : %{name}
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE README

%files javadoc -f .mfiles-javadoc
%doc LICENSE

%changelog
* Mon May 9 2022 caodongxia <caodongxia@h-partners.com> - 1.4-2
- License compliance rectification

* Fri Aug 14 2020 leiju <leiju4@huawei.com> - 1.4-1
- Package init
